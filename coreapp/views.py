from coreapp import app
import datetime
from flask import request
page="""
<DOCTYPE! html>
<html lang="en-US">
<head>
<title>Hello World Page</title>
<meta charset="utf-8">
</head>
<body>
<h1>Enter a date</h1>
<p>Enter a data below and submit and we will find the day of the week on the return page.</p>
{0}

</body>
</html>
"""

page2="""
<DOCTYPE! html>
<html lang="en-US">
<head>
<title>Hello World Page</title>
<meta charset="utf-8">
</head>
<body>
<h1>Date of Week</h1>
<p>
{0}
</p>
</body>
</html>


"""
@app.route('/')
def index():
	datahead = "<form id=\'dateoftheweek\' action=\'/dow\' method=\'get\'>"

	days = "<select name=\'day\'>"
	for i in range(1,32):
		days += "<option value=\'" + str(i) + "\'>" + str(i) + "</option>"
	days += "</select>"

	months = "<select name=\'month\'>"
	for i in range(1,13):
		months += "<option value=\'" + str(i) + "\'>" + str(i) + "</option>"
	months += "</select>"

	years = "<select name=\'year\'>"
	for i in range(2014,2021):
		years += "<option value=\'" + str(i) + "\'>" + str(i) + "</option>"
	years += "</select>"

	dataend = "<input type=\'submit\' value=\'submit\'></form>"
	data = datahead + days + months + years + dataend
	return page.format(data)

@app.route('/dow')
def dow():
	if(len(request.args.keys())==3):
		day=int(request.args.get('day'))
		month=int(request.args.get('month'))
		year=int(request.args.get('year'))
		date=datetime.date(year,month,day).weekday()
		weekdays=['Monday', 'Tuesday', 'Wednesday', 'Thursday',' Friday', 'Saturday', 'Sunday']
		data=weekdays[date]
		return page2.format(data)
	else:
		return page2.format('Error: Missing, incomplete, or incorrect input.<br>400: Bad Request')
